import pandas as pd
import numpy
methylo = []
transcripto = []
integration = []

# Creation of the list of genes regulated by silencers comprising a differentially methylated CpG
DMG = pd.read_csv('Genes_differentially_methylated.tsv', sep = '\t')
methy = DMG.loc [:,'gene']
for i in methy :
	methylo.append(i)

# Creation of the list of differentially expressed genes
with open ('DEG_names.txt', 'r') as f1:
	for li in f1:
		lic = li.strip('\n')
		transcripto.append(lic)
		
# Creation of the genes which are in both lists
for i in methylo:
	for v in transcripto:
		if i == v:
			if i not in integration:
				integration.append(i)
			
for i in integration:
	print (i)

#!/bin/bash
rm output_curl
gene_list=$(cat integration.txt)
#echo $gene_list
for gene in $gene_list
do
	echo $gene
	echo -n https://rest.kegg.jp/find/genes/ > link.txt
	echo -n $gene >> link.txt
	echo -n +hsa >> link.txt
	#echo -n /"hsa"/pathway >> link.txt
	cat link.txt
	link=$(cat link.txt)
	curl $link >> output_curl
done

